package main

import (
	"log"
	"net"
)

// go socket 编程学习资料：https://astaxie.gitbooks.io/build-web-application-with-golang/content/zh/08.1.html

const (
	HOST_AND_PORT = "0.0.0.0:8989"
	NET_TYPE      = "tcp"
)

func main() {
	listen, err := net.Listen(NET_TYPE, HOST_AND_PORT)
	if err != nil {
		log.Fatalf("启动服务失败，准备监听的IP端口=%s，错误信息=%v\n", HOST_AND_PORT, err)
	}
	log.Printf("成功启动服务，正在监听IP端口=%s\n", HOST_AND_PORT)
	for {
		log.Printf("等待新客户端链接......")
		conn, err := listen.Accept()
		if err != nil {
			log.Printf("客户端连接失败，失败信息=%v\n", err)
			continue
		}
		log.Printf("新客户端链接成功，ip=%s", conn.RemoteAddr().String())
		dispatcher := &Dispatcher{
			Conn: conn,
		}
		go dispatcher.process()
	}
}
