# go chat
用 go 实现的高性能海量在线聊天室，整个聊天室采用的是命令行终端模拟

## server 端实现
采用 mvc 架构
+ main: 程序主入口，对应 view 层，读取配置信息，初始化全局变量，等待客户端链接，客户端一旦链接就转发到总控
+ processor: 针对不同的任务，总控会交给不同的 processor 去处理
+ model: 和数据库进行交互的处理工具，以及服务器和客户端消息的封装
+ utils: 各种工具类，接收数据的解包，发送数据的封装，都在这个包里
+ manager: 各种管理类，里面基本都是整个服务器唯一的管理者，例如用户登录管理

## client 端实现
+ main: 程序主入口，会提示客户端使用菜单
+ processor: 程序会根据用户输入的第一层命令，进入第二层命令提示和具体操作
+ manager: 客户端各种管理工具，基本整个客户端唯一

## server 和 client 通用
+ protocol: 客户端和服务端通讯协议，这里直接用 go 写，业界一般使用 google protobuf
+ utils: 各种工具类，服务器和客户端通讯数据的封包和拆解，都在这个包里

## client 和 server 进行通讯的协议设计

通用协议 message

```go
type Message struct {
	MsgType string `json:"msg_type"`
	MsgData string `json:"msg_data"`
}
```
+ MsgType 定义是什么类型的消息
+ MsgData 定义是消息的内容

特别注意：   
~~~
 发送包之前，先发送包的长度，接收者先校验包的长度，然后再真正解析消息体，然后根据 MsgType 去转为对应的消息结构体，拆解包都封装在 utils transfer.go 里面    
~~~

## 实现的功能
1. 会员登录: 一个客户端只能登录一个用户
2. 会员注册: 登录前必须先注册
3. 查看在线用户: 必须先登录
4. 群发消息: 必须先登录
5. 私发消息: 必须先登录
6. 退出系统: 退出系统会通知所有用户此用户以退出


## 编译启动

1. 需要安装 redis 如果安装的和服务端不在同一个机器，请修改 server/model/user_model.go 的 redis ip 低质
2. 编译服务端：`go build -o gochat_server server/main/main.go server/main/dispatcher.go`
3. 编译客户端：`go build -o gochat_client client/main/main.go`
4. 直接二进制启动服务端和客户端即可