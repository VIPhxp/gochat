package utils

import (
	"encoding/binary"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIntBytesChange(t *testing.T) {
	realSize := 91
	sendBuf := make([]byte, 4)
	binary.LittleEndian.PutUint32(sendBuf, uint32(realSize))

	needSize := binary.LittleEndian.Uint32(sendBuf[:4])
	assert.Equal(t, needSize, uint32(realSize))
}
