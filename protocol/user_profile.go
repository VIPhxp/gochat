package protocol

const (
	OnlineStatus  = iota // 在线
	OfflineStatus        // 离线
)

type UserProfile struct {
	Username   int `json:"username"`
	Password   int `json:"password"`
	UserStatus int `json:"online_status"`
}
