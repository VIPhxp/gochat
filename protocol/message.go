package protocol

// 定义客户端和服务器传输的消息类型
const (
	// 登录消息类型
	LoginMsgType = "login_msg"
	// 注册消息类型
	RegisterMsgType = "register_msg"
	// 通知登录消息类型
	NotifyLoginMsgType = "notify_login_msg"
	// 通知登录消息类型
	NotifyLogoutMsgType = "notify_logout_msg"
	// 聊天消息类型
	ChatMsgType = "chat_msg"
)

const (
	Success          = 200
	UserNotExist     = 201
	PasswordError    = 202
	UserExist        = 203
	UserAlreadyLogin = 204
	UnkownError      = 999
)

// 通用消息结构体，消息类型和消息数据
type Message struct {
	MsgType string `json:"msg_type"`
	MsgData string `json:"msg_data"`
}

// 登录请求发送数据
type LoginMsgReq struct {
	Username int `json:"username"`
	Password int `json:"password"`
}

// 登录请求返回数据
type LoginMsgResp struct {
	ErrorCode     int           `json:"error_code"`
	ErrorMsg      string        `json:"error_msg"`
	LoginUsername int           `json:"login_username"`
	OnlineUsers   []UserProfile `json:"online_users"`
}

// 登录请求发送数据
type RegisterMsgReq struct {
	Username int `json:"username"`
	Password int `json:"password"`
}

// 登录请求返回数据
type RegisterMsgResp struct {
	ErrorCode int    `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
}

// 登录请求返回数据
type NotifyLoginMsgResp struct {
	ErrorCode  int         `json:"error_code"`
	OnlineUser UserProfile `json:"online_user"`
}

// 退出请求返回数据
type NotifyLogoutMsgResp struct {
	ErrorCode  int         `json:"error_code"`
	LogoutUser UserProfile `json:"logout_user"`
}

// 聊天请求发送数据
type ChatMsgReq struct {
	SendUserName    int    `json:"send_username"`
	ChatData        string `json:"chat_data"`
	ReceiveUsername int    `json:"receive_username"` // 如果是0表示群聊
}

// 聊天请求返回数据
type ChatMsgResp struct {
	ChatMsgReq
}
