package main

import (
	"fmt"
	"log"
	"net"

	"github.com/clebeg/gochat/client/manager"
	"github.com/clebeg/gochat/client/processor"
)

const (
	HOST_AND_PORT = "0.0.0.0:8989"
	NET_TYPE      = "tcp"
)

func commandPrint() {

	conn, err := net.Dial(NET_TYPE, HOST_AND_PORT)
	if err != nil {
		log.Panicf("连接服务器(%v)发送错误，错误信息：%v", HOST_AND_PORT, err)
	}
	go processor.MsgLoopReceive(conn)

	var loop = true
	for loop {
		fmt.Printf("----------------欢迎使用海量用户聊天系统：请输入：-------------------------\n")
		fmt.Printf("\t\t\t\t 1 会员登录\n")
		fmt.Printf("\t\t\t\t 2 会员注册\n")
		fmt.Printf("\t\t\t\t 3 查看在线用户\n")
		fmt.Printf("\t\t\t\t 4 群发消息\n")
		fmt.Printf("\t\t\t\t 5 私发消息\n")
		fmt.Printf("\t\t\t\t 6 退出系统\n")
		var commandNum int
		_, err := fmt.Scanf("%d\n", &commandNum)
		if err != nil {
			fmt.Printf("输入信息有误，请输入数字: %s\n", err)
		}
		switch commandNum {
		case 1:
			fmt.Println("开始登录")
			up := processor.UserProcessor{
				Conn: conn,
			}
			up.DoLogin()
		case 2:
			fmt.Println("开始注册")
			up := processor.UserProcessor{
				Conn: conn,
			}
			up.DoRegister()
		case 3:
			if manager.CurLoginUser == 0 {
				fmt.Println("还没有用户登录，必须先登录！")
				continue
			}
			processor.ShowOnlineUsers()
		case 4:
			if manager.CurLoginUser == 0 {
				fmt.Println("还没有用户登录，必须先登录！")
				continue
			}
			up := processor.ChatProcessor{
				Conn: conn,
			}
			up.DoGroupChat()
		case 5:
			if manager.CurLoginUser == 0 {
				fmt.Println("还没有用户登录，必须先登录！")
				continue
			}
			up := processor.ChatProcessor{
				Conn: conn,
			}
			up.DoOneChat()
		case 6:
			loop = false
		default:
			fmt.Printf("请输入[1-6]\n")
		}
	}

}

func main() {
	commandPrint()
}
