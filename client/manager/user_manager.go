package manager

import "github.com/clebeg/gochat/protocol"

var CurLoginUser = int(0)

// 客户端也需要保留所有的在线用户
var OnlineUserProfiles = make(map[int]*protocol.UserProfile, 128)

func AddOnlineUser(username int, userProfile protocol.UserProfile) {
	OnlineUserProfiles[username] = &userProfile
}

func RemoveOnlineUser(username int) {
	delete(OnlineUserProfiles, username)
}
